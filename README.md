# python-compilator

This repo contains all sources for **Compilators** course.

## Tree structure

| Folder  | Purpose    |
|---------|------------|
| src/s*  | All series |
| src/tp* | All tps    |

# Author

<table>
   <tr>
      <td>
         <a href="https://gitlab.com/fridezlucas"><img width=140px src="https://secure.gravatar.com/avatar/72c1469bf815bd4e0a858341571d5111?s=800&d=identicon"><br>
         Fridez Lucas</a>
      </td>
   </tr>
</table>

Developped during Bsc 3rd year (2020-2021).