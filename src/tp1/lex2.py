import ply.lex as lex

# region Compilator

# Define all tokens
tokens = (
    "ADD_OP",
    "MUL_OP",
    "NUMBER"
)

def t_ADD_OP(t):
	r'\+|-'
	return t
	
def t_MUL_OP(t):
	r'\*|/'
	return t

def t_NUMBER(t):
	r'\d+(\.\d+)?'
	try:
		t.value = float(t.value)    
	except ValueError:
		print ("Line %d: Problem while parsing %s!" % (t.lineno,t.value))
		t.value = 0
	return t


def t_newline(t):
    r"\n+"
    t.lexer.lineno += len(t.value)


# Ignore tokens
t_ignore = " \t"

def t_error(t):
    """On error
    """
    print(f"Illegal character '{t.value}'.")


# Build
lex.lex()

# endregion

# region Main

if __name__ == "__main__":
    import sys
    prog = open(sys.argv[1]).read()

    lex.input(prog)

    while True:
        tok = lex.token()
        if not tok:
            break
        print(f"line {tok.lineno}: {tok.type}({tok.value})")

# endregion
