# -*- coding: utf-8 -*-

from ply import lex
import sys

def debug(f):
    """Infos ! La fonction debug(f) est un décorateur
    """
    def inner(*args):
        global indent
        
        #indentation printable
        strIndent = "|  " * indent

        print(f"{LATok.value.ljust(4) if LATok else 'EOF '}{strIndent}{f.__name__}: {args[0] if args != () else ''}")
        
        # indentation
        indent += 1
        value = f(*args)
        indent -= 1
        return value
    return inner


tokens = [
    "identifier"
]

literals = '()+'
t_identifier = r"\d+"

def t_error(t):
    print("Bad char!")

@debug
def token(t):
    global LATok # Look-ahead token

    if not LATok:
        return t == 'EOF' #case of end of tokens, we return True
    if LATok.type != t :  #case of inexpected token
        return False

    LATok = lex.token() #case of expected token, we consume the current and go for the next token
    return True

@debug
def require(found):
    if found:
        return True
    error("Error while parsing near token %s!" %LATok.value)

@debug
def input():
    return expression() and require(token("EOF"))

@debug
def expression():
    return term() and require(rest_expression())

@debug
def term():
    return token("identifier") or parenth_expr()

@debug
def parenth_expr():
    return token("(") and require(expression()) and require(token(")"))

@debug
def rest_expression():
    return (token("+") and require(expression())) or True


lex.lex()

def parse(s):
    global LATok
    global indent
    indent = 0
    print("** parsing: ", s)
    lex.input(s)
    LATok = lex.token()  # Read the first token from lexical analyser
    result = input()  # Start parsing
    print("** result: ", result)

if __name__ == '__main__':
    input_str = "12+3"
    parse(input_str)
