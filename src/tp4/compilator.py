
import AST
from AST import addToClass

operations = {
    "+": "ADD",
    "-": "SUB",
    "*": "MUL",
    "/": "DIV",
}


@addToClass(AST.ProgramNode)
def compile(self):
    compiled = ""
    for c in self.children:
        compiled += c.compile()
    return compiled


@addToClass(AST.TokenNode)
def compile(self):
    compiled = ""

    if isinstance(self.tok, str):
        compiled += f"PUSHV {self.tok}\n"
    else:
        compiled += f"PUSHC {self.tok}\n"

    return compiled


@addToClass(AST.OpNode)
def compile(self):
    compiled = ""
    for c in self.children:
        compiled += c.compile()

    compiled += f"{operations[self.op]}\n"
    return compiled


@addToClass(AST.PrintNode)
def compile(self):
    compiled = ""
    print(self.children)
    compiled += f"{self.children[0].compile()}"
    compiled += "PRINT\n"
    return compiled


@addToClass(AST.AssignNode)
def compile(self):
    compiled = ""
    compiled += self.children[1].compile()
    compiled += f"SET {self.children[0].tok}\n"
    return compiled


if __name__ == "__main__":
    from parser5 import parse
    import sys
    import os
    prog = open(sys.argv[1]).read()
    ast = parse(prog)

    compiled = ast.compile()
    name = os.path.splitext(sys.argv[1])[0] + ".vm"
    outfile = open(name, "w")
    outfile.write(compiled)
    outfile.close()
    print(f"Wrote output to {name}")
