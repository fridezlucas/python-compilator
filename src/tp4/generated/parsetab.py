
# parsetab.py
# This file is automatically generated. Do not edit.
# pylint: disable=W,C,R
_tabversion = '3.10'

_lr_method = 'LALR'

_lr_signature = "leftADD_OPleftMUL_OPADD_OP IDENTIFIERS LPARENT MUL_OP NUMBER PRINT RPARENT WHILEprogram : statementprogram : statement ';' programstatement : assignmentstatement : structurestatement : PRINT expressionstructure : WHILE expression '{' program '}' assignment : IDENTIFIERS '=' expression expression : IDENTIFIERS expression : NUMBERexpression : expression ADD_OP expression\n        | expression MUL_OP expressionexpression : ADD_OP expressionexpression : LPARENT expression RPARENT"
    
_lr_action_items = {'PRINT':([0,8,22,],[5,5,5,]),'IDENTIFIERS':([0,5,7,8,12,13,14,17,18,22,],[6,10,10,6,10,10,10,10,10,6,]),'WHILE':([0,8,22,],[7,7,7,]),'$end':([1,2,3,4,9,10,11,16,19,21,23,24,25,27,],[0,-1,-3,-4,-5,-8,-9,-2,-12,-7,-10,-11,-13,-6,]),'}':([2,3,4,9,10,11,16,19,21,23,24,25,26,27,],[-1,-3,-4,-5,-8,-9,-2,-12,-7,-10,-11,-13,27,-6,]),';':([2,3,4,9,10,11,19,21,23,24,25,27,],[8,-3,-4,-5,-8,-9,-12,-7,-10,-11,-13,-6,]),'NUMBER':([5,7,12,13,14,17,18,],[11,11,11,11,11,11,11,]),'ADD_OP':([5,7,9,10,11,12,13,14,15,17,18,19,20,21,23,24,25,],[12,12,17,-8,-9,12,12,12,17,12,12,-12,17,17,-10,-11,-13,]),'LPARENT':([5,7,12,13,14,17,18,],[13,13,13,13,13,13,13,]),'=':([6,],[14,]),'MUL_OP':([9,10,11,15,19,20,21,23,24,25,],[18,-8,-9,18,18,18,18,18,-11,-13,]),'{':([10,11,15,19,23,24,25,],[-8,-9,22,-12,-10,-11,-13,]),'RPARENT':([10,11,19,20,23,24,25,],[-8,-9,-12,25,-10,-11,-13,]),}

_lr_action = {}
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = {}
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'program':([0,8,22,],[1,16,26,]),'statement':([0,8,22,],[2,2,2,]),'assignment':([0,8,22,],[3,3,3,]),'structure':([0,8,22,],[4,4,4,]),'expression':([5,7,12,13,14,17,18,],[9,15,19,20,21,23,24,]),}

_lr_goto = {}
for _k, _v in _lr_goto_items.items():
   for _x, _y in zip(_v[0], _v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = {}
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> program","S'",1,None,None,None),
  ('program -> statement','program',1,'p_program_statement','parser5.py',16),
  ('program -> statement ; program','program',3,'p_program_recursive','parser5.py',21),
  ('statement -> assignment','statement',1,'p_statement_assignement','parser5.py',26),
  ('statement -> structure','statement',1,'p_statement_structure','parser5.py',30),
  ('statement -> PRINT expression','statement',2,'p_statement_printexpression','parser5.py',34),
  ('structure -> WHILE expression { program }','structure',5,'p_structure','parser5.py',38),
  ('assignment -> IDENTIFIERS = expression','assignment',3,'p_assignment','parser5.py',43),
  ('expression -> IDENTIFIERS','expression',1,'p_expression_var','parser5.py',49),
  ('expression -> NUMBER','expression',1,'p_expression_num','parser5.py',55),
  ('expression -> expression ADD_OP expression','expression',3,'p_expression_op','parser5.py',60),
  ('expression -> expression MUL_OP expression','expression',3,'p_expression_op','parser5.py',61),
  ('expression -> ADD_OP expression','expression',2,'p_expression_uadd_min_op','parser5.py',66),
  ('expression -> LPARENT expression RPARENT','expression',3,'p_expression_parentheses_op','parser5.py',74),
]
