import sys
import re

# region Logic


def getLinesAccordingRegexInFile(regex, file):
    matched = []

    with open(file) as f:
        content = f.read()
        matched = re.findall(regex, content)

    return matched


def printLines(lines):
    i = 0
    t = len(lines)
    for l in lines:
        i += 1
        print(f"[{i}/{t}] : {l}")
# endregion


# region main
def main(regex, file):
    print(f"Analysis file\t'{file}'\nwith regex\t'{regex}'")
    lines = getLinesAccordingRegexInFile(regex, file)
    printLines(lines)


# endregion

if __name__ == "__main__":
    if(len(sys.argv) != 3):
        print("[Error] : This script need exactly 2 arguments", file=sys.stderr)
    else:
        main(sys.argv[1], sys.argv[2])
