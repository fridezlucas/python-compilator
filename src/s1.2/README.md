# Exercice 1

## Contiennent des nombres avec une virgule en notation scientifique.

```py
python3 ex1_filter.py "[+-]?\d[eE]\d*" "source.txt"
```

## Commencent par un espace ou une tabulation.

```py
python3 ex1_filter.py "^[\s\t]" "source.txt"
```

## Ne contiennent que des caractères alphanumériques.

```py
python3 ex1_filter.py "^[a-zA-Z0-9_]*$" "source.txt"
```

## Sont non vides.

```py
python3 ex1_filter.py "^.+$" "source.txt"
```

## Contiennent un mot commençant par une majuscule.

```py
python3 ex1_filter.py "[A-Z][a-z]+" "source.txt"
```

# Exercice 2

## Les commentaires "//" d’un fichier java.

```py
python3 ex2_find.py "\/\/.*" "source2.txt"
```

## Les adresses mail d’un fichier texte.

```py
python3 ex2_find.py "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}" "source2.txt"
```

## Les URLs d’un fichier html

```py
python3 ex2_find.py "https?:\/\/[a-z0-9]{2,}\.[a-z]{2,}[\/a-z0-9]*" "source2.txt"
```
