import ply.yacc as yacc
from lex4 import tokens
import AST

operations = {
    "+": lambda x, y: x+y,
    "-": lambda x, y: x-y,
    "*": lambda x, y: x*y,
    "/": lambda x, y: x/y,
}

vars = {}

def p_program_statement(p):
    '''program : statement'''
    p[0] = AST.ProgramNode(p[1])


def p_program_recursive(p):
    '''program : statement ';' program'''
    p[0] = AST.ProgramNode([p[1]] + p[3].children)


def p_assignment(p):
    '''assignment : IDENTIFIERS '=' expression'''
    vars[p[1]] = p[3]
    p[0] = AST.AssignNode([AST.TokenNode(p[1]), p[3]])


def p_expression_var(p):
    ''' expression : IDENTIFIERS '''
    #p[0] = vars[p[1]]
    p[0] = AST.TokenNode(p[1])


def p_statement(p):
    '''statement : assignment
        | expression'''
    p[0] = p[1]


def p_expression_num(p):
    'expression : NUMBER'
    p[0] = AST.TokenNode(p[1])


def p_expression_op(p):
    '''expression : expression ADD_OP expression
        | expression MUL_OP expression'''
    p[0] = AST.OpNode(p[2], [p[1], p[3]])


def p_expression_uadd_min_op(p):
    'expression : ADD_OP expression'
    if(p[1] == "+"):
        p[0] = AST.TokenNode(p[2].tok)
    else:
        p[0] = AST.TokenNode(-p[2].tok)


def p_expression_parentheses_op(p):
    '''expression : LPARENT expression RPARENT'''
    p[0] = p[2]


def p_error(p):
    print(f"Syntax error in line {p.lineno}")
    yacc.errok()


precedence = (
    ("left", "ADD_OP"),
    ("left", "MUL_OP")
)

yacc.yacc(outputdir="generated")

if __name__ == "__main__":
    import sys
    prog = open(sys.argv[1]).read()
    result = yacc.parse(prog)
    print(result)
    import os
    graph = result.makegraphicaltree()
    name = os.path.splitext(sys.argv[1])[0] + "-ast.pdf"
    graph.write_pdf(name)
    print(f"wrote ast to {name}")
