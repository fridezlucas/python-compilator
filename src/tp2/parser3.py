import ply.yacc as yacc
from lex4 import tokens

operations = {
    "+": lambda x, y: x+y,
    "-": lambda x, y: x-y,
    "*": lambda x, y: x*y,
    "/": lambda x, y: x/y,
}

vars = {}


def p_program(p):
    '''program : statement
        | statement ';' program'''
    try:
        p[0] = p[3]
    except:
        p[0] = p[1]


def p_assignment(p):
    '''assignment : IDENTIFIERS '=' expression'''
    vars[p[1]] = p[3]
    p[0] = p[3]


def p_expression_var(p):
    ''' expression : IDENTIFIERS '''
    p[0] = vars[p[1]]


def p_statement(p):
    '''statement : assignment
        | expression'''
    p[0] = p[1]


def p_expression_num(p):
    'expression : NUMBER'
    p[0] = p[1]


def p_expression_op(p):
    '''expression : expression ADD_OP expression
        | expression MUL_OP expression'''
    p[0] = operations[p[2]](p[1], p[3])


def p_expression_uadd_min_op(p):
    'expression : ADD_OP expression'
    if(p[1] == "+"):
        p[0] = p[2]
    else:
        p[0] = -p[2]


def p_expression_parentheses_op(p):
    '''expression : LPARENT expression RPARENT'''
    p[0] = p[2]


def p_error(p):
    print(f"Syntax error in line {p.lineno}")
    yacc.errok()


precedence = (
    ("left", "ADD_OP"),
    ("left", "MUL_OP")
)

yacc.yacc(outputdir="generated")

if __name__ == "__main__":
    import sys
    prog = open(sys.argv[1]).read()
    result = yacc.parse(prog)
    print(result)