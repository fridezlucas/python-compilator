#region Constants

stateMachine = {
    "s_START": {
        # add programmatically 0 to 9 posibility to go in S_DIGIT
        "0": "s_START_ZERO",
        "+": "s_SIGN",
        "-": "s_SIGN"
    },
    "s_SIGN": {
        # add programmatically 0 to 9 posibility to go in S_DIGIT
        ".": "s_DECIMAL",
    },
    "s_DECIMAL": {
        # add programmatically 0 to 9 posibility to go in S_DIGIT
        "\n": "s_END" 
    },
    "s_START_ZERO": {
        # add programmatically 1 to 9 posibility to go in S_DIGIT
        ".": "s_DECIMAL",
    },
    "s_DIGIT": {
        # add programmatically 0 to 9 posibility to go in S_DIGIT
        ".": "s_DECIMAL",
        "\n": "s_END" 
    },
}

stateMachine["s_START_ZERO"].update({str(n): "s_DIGIT" for n in range(1, 10)})
stateMachine["s_START"].update({str(n): "s_DIGIT" for n in range(1, 10)})
stateMachine["s_SIGN"].update({str(n): "s_DIGIT" for n in range(0, 10)})
stateMachine["s_DIGIT"].update({str(n): "s_DIGIT" for n in range(0, 10)})
stateMachine["s_DECIMAL"].update({str(n): "s_DIGIT" for n in range(0, 10)})

#endregion

#region Exception

class StateMachineError(Exception):
    pass

#endregion

#region Logic

def validate(s):

    actualState = "s_START"
    itString = iter(s)

    while actualState != "s_END":
        token = next(itString)
        possibleTransitions = stateMachine[actualState]

        if token in possibleTransitions.keys():
            actualState = possibleTransitions[token]
        else:
            raise StateMachineError
    return True

#endregion

#region main

def printResult(s, res):
    s = s.replace("\n", "")
    endSentence = "is valid" if res else "is not valid"
    print(f"{s}\t{endSentence}")

def main():
    strToTests = [
        "1\n",
        "1.3\n",
        "+3.99\n",
        "-445.2\n",
        "345.21\n",
        "0.5\n",
        "-0.2\n",
        "0004\n", # must raise error
        "4.\n",
        "400\n"
    ]

    for s in strToTests:
        try:
            validate(s)
            printResult(s, True)
        except StateMachineError:
            printResult(s, False)

#endregion main

if __name__ == "__main__":
    main()